import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Autor: Adrian Pinz
 *
 * Datum: 27.01.2023
 */
public class AsciiInputStream extends FileInputStream {
    public AsciiInputStream(File name) throws FileNotFoundException {
        super(name);
    }

    public int countLines(File file) throws IOException {
        try (AsciiInputStream stream = new AsciiInputStream(file)) {

            byte[] b = stream.readAllBytes();
            int count = 1;
            for (byte i : b) {
                if (i == 10) {
                    count++;
                }
            }
            return count;
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("File not found: " + e);
        } catch (IOException e) {
            throw new IOException("Fehler" + e);
        }
    }

    public List<Integer> readLine() throws IOException {
        List<Integer> lst = new ArrayList<>();
        while (!lst.contains(10)) {
            int read = this.read();
            if (read == -1) {
                break;
            }

            lst.add(read);
        }
        return lst;
    }
}