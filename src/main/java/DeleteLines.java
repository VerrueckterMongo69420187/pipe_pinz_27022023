import java.io.*;
import java.util.*;

/**
 * Autor: Adrian Pinz
 *
 * Datum: 27.01.2023
 */

public class DeleteLines {
    public static TreeMap<Integer, List<Integer>> delete(String[] del) throws IOException {
        File inputFile = new File(del[0]);
        File outputFile = new File(del[1]);
        TreeMap<Integer, List<Integer>> map = new TreeMap<>();
        try (AsciiInputStream inputStream = new AsciiInputStream(inputFile); FileOutputStream outputStream = new FileOutputStream(outputFile)) {
            for (int i = 0; i < inputStream.countLines(inputFile); i++) {
                map.put(i, inputStream.readLine());
            }
            for (String deletion : Arrays.stream(del.clone()).toList().subList(2, del.clone().length)) {
                if (deletion.length() >= 3 && deletion.contains("-")) {
                    int von = Integer.parseInt(deletion.split("-")[0]);
                    int bis = Integer.parseInt(deletion.split("-")[1]);
                    if (von>bis){
                        throw new IllegalArgumentException(deletion + " ist nicht im Format 'von - bis' geschrieben worden");
                    }
                    Iterator<Map.Entry<Integer, List<Integer>>> iterator = map.entrySet().iterator();
                    while (iterator.hasNext()) {
                        Map.Entry<Integer, List<Integer>> i = iterator.next();
                        if (i.getKey()<=bis && i.getKey()>=von) {
                            iterator.remove();
                        }
                    }
                } else if (deletion.length() >= 1 && !deletion.contains("-")) {
                    Iterator<Map.Entry<Integer, List<Integer>>> iterator = map.entrySet().iterator();
                    while (iterator.hasNext()) {
                        Map.Entry<Integer, List<Integer>> i = iterator.next();
                        if (i.getKey() == Integer.parseInt(deletion)) {
                            iterator.remove();
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Falsche Eingabe der Loeschparameter");
                }
            }
            for (Integer i : map.keySet()) {
                for (Integer j : map.get(i)) {
                    if (j != null) {
                        outputStream.write(j);
                    }

                }
            }
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("File not found: " + e);
        } catch (IOException e) {
            throw new IOException("Fehler" + e);
        }
        return map;
    }

    //public static void makeFile(String filename) throws IOException {
    //    try (PipedInputStream p_in = new PipedInputStream();
    //         PipedOutputStream p_out = new PipedOutputStream(p_in);
    //         FileOutputStream outputStream = new FileOutputStream(filename)){
    //
    //        StreamCopier.copyByte(System.in, p_out);
    //        StreamCopier.copyBuffered(p_in, outputStream, 4);
    //    } catch (IOException e) {
    //        throw new IOException("Error: " + e);
    //    }
    //}

    public static void main(String[] args) throws IOException {
//        DeleteLines.makeFile("src/main/resources/input.txt");
        DeleteLines.delete(args);
    }
}