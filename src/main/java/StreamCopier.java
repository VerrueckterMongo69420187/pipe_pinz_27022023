/**
 * Autor: Adrian Pinz
 * <p>
 * Datum: 27.01.2023
 */

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class StreamCopier {

    public static void copyByte(InputStream source, OutputStream output) throws IOException {
        int data;
        while ((data = source.read()) != -1)

            output.write(data);
    }

    public static void copyBuffered(InputStream source, OutputStream output, int size) throws IOException {
        int bytesRead;
        byte[] buffer = new byte[size];
        while ((bytesRead = source.read(buffer)) != -1)
            output.write(buffer, 0, bytesRead);
    }

    public static void main(String[] args) throws IOException {
        try (PipedInputStream pipedInputStream = new PipedInputStream();
             PipedOutputStream pipedOutputStream = new PipedOutputStream(pipedInputStream);
             FileOutputStream outputStream = new FileOutputStream("src/main/resources/outputStreamCopier.txt")) {

            StreamCopier.copyByte(System.in, pipedOutputStream);
            StreamCopier.copyBuffered(pipedInputStream, outputStream, 4);

        } catch (IOException e) {
            throw new IOException("Fehler" + e.getStackTrace());
        }
    }

}


